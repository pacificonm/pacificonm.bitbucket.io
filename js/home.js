const socialMedia = {
	facebook:	"https://www.facebook.com/nicholas.marconi.773",
	instagram: 	"https://www.instagram.com/pacificonm/",
	linkedin: 	"https://www.linkedin.com/in/nicholas-marconi-0a4176198/",
	bitbucket:	"https://bitbucket.org/%7B7457277a-0383-4207-a809-a5dbc67cf7ec%7D/",
	github:		"https://github.com/Pacificonm?tab=repositories"
};

function connectClick(key){
	let win = window.open(socialMedia[key], '_blank');
  	win.focus();
}

function infoPageClick(fileName){
	let file = fileName + ".html";
	
    document.location.href = file;
}

function resumePageClick(){
	if($(document).width() <= 700){
		window.open('downloads/resume.pdf');
	}
	else{
		document.location.href = 'resume.html';
	}
}